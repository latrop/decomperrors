#! /usr/bin/env python

from os import path
from os import remove
from random import gauss
import subprocess
import time

class PSF:
    def __init__(self, psfStatFile):
        for line in open(psfStatFile):
            if line.startswith("<FWHM>"):
                params = line.split()
                self.FWHM = float(params[2])
                self.FWHMSeigma = float(params[4])
                continue
            if line.startswith("<Beta>"):
                params = line.split()
                self.beta = float(params[2])
                self.betaSigma = float(params[4])
                continue
            if line.startswith("<Q>"):
                params = line.split()
                self.q = float(params[2])
                self.qSigma = float(params[4])
                continue
            if line.startswith("<PA>"):
                params = line.split()
                self.PA = float(params[2])
                self.PASigma = float(params[4])
                continue
        print("PSF loaded")
    
    def create_precise_psf(self, directory):
        modelName = path.join(directory, "psf.galfit")
        psfFitsName = path.join(directory, "pri_psf.fits")
        fout = open(modelName, "w")
        fout.truncate(0)
        fout.write("==============================================================\n")
        fout.write("A) none           # Input data image (FITS file)\n")
        fout.write("B) %s     # Output data image block\n" % psfFitsName)
        fout.write("C) none                # Sigma image name (made from data if blank or 'none') \n")
        fout.write("D) none                # Input PSF image and (optional) diffusion kernel\n")
        fout.write("E) 1                   # PSF fine sampling factor relative to data \n")
        fout.write("F) none                # Bad pixel mask (FITS image or ASCII coord list)\n")
        fout.write("G) none                # File with parameter constraints (ASCII file) \n")
        fout.write("H) 1 51 1 51           # Image region to fit (xmin xmax ymin ymax)\n")
        fout.write("I) 50 50               # Size of the convolution box (x y)\n")
        fout.write("J) 28.24               # Magnitude photometric zeropoint \n")
        fout.write("K) 0.396  0.396        # Plate scale (dx dy)   [arcsec per pixel]\n")
        fout.write("O) regular             # Display type (regular, curses, both)\n")
        fout.write("P) 1                   # Choose: 0=optimize, 1=model, 2=imgblock, 3=subcomps\n")

        fout.write("Component number: 1\n")
        fout.write("0) moffat                #  Component type\n")
        fout.write("1) 26 26 1 1  #  Position x, y\n")
        fout.write("3) 15         1       # total magnitude     \n")
        fout.write("4) %1.3f      1       #   FWHM               [pixels]\n" % (self.FWHM))
        fout.write("5) %1.3f      1       # powerlaw   \n" % (self.beta))
        fout.write("9) %1.3f      1       # axis ratio (b/a)\n" % (self.q))   
        fout.write("10) %1.3f     1       # position angle (PA)  [Degrees: Up=0, Left=90]\n" % (self.PA))
        fout.write("Z) 0\n")
        fout.close()
        subprocess.call("galfit %s >/dev/null" % (modelName), shell=True)
        if path.exists(modelName):
            remove(modelName)
        return psfFitsName

    def create_erroneous_psf(self, directory):
        modelName = path.join(directory, "psf.galfit")
        psfFitsName = path.join(directory, "pri_err.fits")
        fout = open(modelName, "w")
        fout.truncate(0)
        fout.write("==============================================================\n")
        fout.write("A) none           # Input data image (FITS file)\n")
        fout.write("B) %s     # Output data image block\n" % psfFitsName)
        fout.write("C) none                # Sigma image name (made from data if blank or 'none') \n")
        fout.write("D) none                # Input PSF image and (optional) diffusion kernel\n")
        fout.write("E) 1                   # PSF fine sampling factor relative to data \n")
        fout.write("F) none                # Bad pixel mask (FITS image or ASCII coord list)\n")
        fout.write("G) none                # File with parameter constraints (ASCII file) \n")
        fout.write("H) 1 51 1 51           # Image region to fit (xmin xmax ymin ymax)\n")
        fout.write("I) 50 50               # Size of the convolution box (x y)\n")
        fout.write("J) 28.24               # Magnitude photometric zeropoint \n")
        fout.write("K) 0.396  0.396        # Plate scale (dx dy)   [arcsec per pixel]\n")
        fout.write("O) regular             # Display type (regular, curses, both)\n")
        fout.write("P) 1                   # Choose: 0=optimize, 1=model, 2=imgblock, 3=subcomps\n")

        fout.write("Component number: 1\n")
        fout.write("0) moffat                #  Component type\n")
        fout.write("1) 26 26 1 1  #  Position x, y\n")
        fout.write("3) 15         1       # total magnitude     \n")
        fout.write("4) %1.3f      1       #   FWHM    [pixels]\n" % (gauss(self.FWHM, self.FWHMSeigma)))
        fout.write("5) %1.3f      1       # powerlaw   \n" % (gauss(self.beta, self.betaSigma)))
        fout.write("9) %1.3f      1       # axis ratio (b/a)\n" % (gauss(self.q, self.qSigma)))   
        fout.write("10) %1.3f     1       # position angle (PA)\n" % (gauss(self.PA, self.PASigma)))
        fout.write("Z) 0\n")
        fout.close()
        subprocess.call("galfit %s >/dev/null" % (modelName), shell=True)
        if path.exists(modelName):
            remove(modelName)
        return psfFitsName
