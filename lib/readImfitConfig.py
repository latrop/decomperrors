#! /usr/bin/env python

import tempfile
from os import path
import subprocess


def parse_imfit_line(line):
    """ Function parses line of imfit data file and
    returns parameters"""
    params = line.split()
    name = params[0]
    value = float(params[1]) # Value of the parameter is the second entry of the line
    # (after the name)
    # Lets now find range of values. Its have to contain the coma and must
    # be the second enrty (otherwise imfin wont work).
    # Some parameters can be fixed, so we have to check this possibility at first
    if (len(params) == 2) or ("fixed" in params[2]) :
        # No bounds specified at all or fixed value
        lowerLim = upperLim = None
    else:
        rangeParams = params[2].split(",")
        lowerLim = float(rangeParams[0])
        upperLim = float(rangeParams[1])
	    
    return ImfitParameter(name, value, lowerLim, upperLim)


class ImfitParameter(object):
    """ Just a container of parameter instance:
    parameter name, value and its range"""
    def __init__(self, name, value, lowerLim, upperLim):
        self.name = name
        self.value = value
        self.lowerLim = lowerLim
        self.upperLim = upperLim
        if lowerLim is None:
            self.fixed = True
        else:
            self.fixed = False
    def tostring(self, fixAll):
        if fixAll or self.fixed:
            return "{:12}{:6.2f}     fixed\n".format(self.name, self.value)
        else:
            return "{:12}{:6.2f}{:12.2f},{:1.2f}\n".format(self.name, self.value, self.lowerLim, self.upperLim)
    def change_value(self, newValue):
        self.value = newValue
        if not self.fixed:
            if self.lowerLim > newValue:
                self.lowerLim = newValue - newValue*0.001
            if self.upperLim < newValue:
                self.upperLim = newValue + newValue*0.001


class ImfitFunction(object):
    """ Class represents imfit function with
    all its parameters their ranges"""
    def __init__(self, funcName, ident):
        # ident is a unical number of the function
        # funcName is just a type of the function and it can be 
        # the same for different galaxy components
        self.name = funcName
        self.ident = ident
        self.uname = "%s.%i" % (funcName, ident) # func unique name
        self.params = []
    def add_parameter(self, newParameter):
        self.params.append(newParameter)
    def num_of_params(self):
        return len(self.params)
    def get_par_by_name(self, name):
        for par in self.params:
            if par.name == name:
                return par


class ImfitModel(object):
    """Imfit functions and their parameters"""
    def __init__(self, modelFileName):
        print("Reading '%s':" % (modelFileName))
        self.modelFileName = modelFileName
        # Read imfit input file
        self.listOfFunctions = []
        self.numberOfParams = 0
        funcName = None
        ident = -1
        for line in open(modelFileName):
            sLine = line.strip()
            if sLine.startswith("#"):
                # It is a comment line, just skip it
                continue
            if len(sLine) == 0:
                "Empty line"
                continue
            if "#" in sLine:
                # Drop the comment part of the line if exists
                sLine = sLine[:sLine.index("#")].strip()
            if sLine.startswith("X0"):
                x0 = parse_imfit_line(sLine)
            elif sLine.startswith("Y0"):
                y0 = parse_imfit_line(sLine)
            elif sLine.startswith("FUNCTION"):
                # New function is found.
                ident += 1
                # If we are working already with some function, then
                # the list of parameters for this function is over and we can
                # add it to the function list
                if funcName is not None:
                    self.listOfFunctions.append(currentFunction)
                funcName = sLine.split()[1]
                currentFunction = ImfitFunction(funcName, ident)
                currentFunction.add_parameter(x0)
                currentFunction.add_parameter(y0)
                self.numberOfParams += 2
            else:
                # If line does not contain nor coordinates nor function name
                # then in has to be a parameter line
                param = parse_imfit_line(sLine)
                currentFunction.add_parameter(param)
                self.numberOfParams += 1
        # append the last function
        self.listOfFunctions.append(currentFunction)
        # Print some statistics
        print("  %i functions found (%i parameters)\n" % (len(self.listOfFunctions), self.numberOfParams))

    def get_func_by_uname(self, uname):
        for func in self.listOfFunctions:
            if uname == func.uname:
                return func

    def create_input_file(self, fileName, fixAll=False):
        fout = open(fileName, "w")
        fout.truncate(0)
        for func in self.listOfFunctions:
            fout.write(func.get_par_by_name("X0").tostring(fixAll))
            fout.write(func.get_par_by_name("Y0").tostring(fixAll))
            fout.write("FUNCTION " + func.name+"\n")
            for par in func.params[2:]:
                fout.write(par.tostring(fixAll))
        fout.close()
        # print("Model was saved to '%s'\n" % (fileName))
        return fileName


    def check_boundaries(self, resModel):
        """ Method takes other model object and checks if its parameter
        values are close to the parameter limiths of the self model"""
        badParams = []
        for selfFunc, resFunc in zip(self.listOfFunctions, resModel.listOfFunctions):
            for selfParam, resParam in zip(selfFunc.params, resFunc.params):
                if selfParam.fixed:
                    # Fixed parameter, so it does not have boundaries. Nothing to check.
                    continue
                parRange = selfParam.upperLim - selfParam.lowerLim
                eps = parRange / 1000.0
                if (abs(resParam.value-selfParam.upperLim)<eps) or (abs(resParam.value-selfParam.lowerLim)<eps):
                    badParams.append("%s(%i): %s" % (selfFunc.name, selfFunc.ident, selfParam.name))
        return badParams

    def model_to_text(self, textFile):
        """ Method saves current values of model parameters to a text file """
        if not path.exists(textFile):
            fout = open(textFile, "w", buffering=1)
            # Create a header as a first line of a file
            fout.write("# ")
            for func in self.listOfFunctions:
                for param in func.params:
                    fout.write("  %s.%s" % (func.uname, param.name))
            fout.write("\n")
        else:
            fout = open(textFile, "a", buffering=1)
        for func in self.listOfFunctions:
            for param in func.params:
                fout.write("  %9.3f" % param.value)
        fout.write("\n")

    def create_model_image(self, outFileName, psfName):
        print("Creating %s model" % outFileName)
        dirName = path.split(outFileName)[0]
        configName = path.join(dirName, "input.imfit")
        self.create_input_file(fileName=configName)
        xSize = int(self.listOfFunctions[0].get_par_by_name("X0").value * 2)
        ySize = int(self.listOfFunctions[0].get_par_by_name("Y0").value * 2)
        callString = "makeimage %s " % configName
        callString += "--ncols %i --nrows %i" % (xSize, ySize)
        callString += " -o %s --psf %s" % (outFileName, psfName)
        callString += " >/dev/null"
        subprocess.call(callString, shell=True)
