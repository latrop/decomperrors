#! /usr/bin/env python

import sys
import tempfile
import argparse
from os import path
from os import mkdir
from os import remove
import subprocess
from itertools import repeat
from multiprocessing import Pool
import time


from numpy import random
from astropy.io import fits

from lib.readImfitConfig import ImfitModel
from lib.psf import PSF


def add_noise(imgFileName, outFileName, noiseStd):
    hdu = fits.open(imgFileName)
    data = hdu[0].data
    noiseGauss = random.normal(0, noiseStd, data.shape)
    noisePoisson = random.poisson(data)
    data += (noiseGauss+noisePoisson)
    hdu.writeto(outFileName, clobber=True)


def run_model_decomposition(inputImfitModel, psf, noiseStd):
    tempDir = tempfile.TemporaryDirectory(dir="workDir")
    # create precise and errorneous psfs
    psfPriFitsName = psf.create_precise_psf(tempDir.name)
    psfErrFitsName = psf.create_erroneous_psf(tempDir.name)

    # create model and add noise to it
    cleanModelFitsName = path.join(tempDir.name, "model_clean.fits")
    noisedModelFitsName = path.join(tempDir.name, "model_noised.fits")
    inputImfitModel.create_model_image(cleanModelFitsName, psfPriFitsName)
    add_noise(cleanModelFitsName, noisedModelFitsName, noiseStd)

    # run decomposition procedure
    outImfitName = path.join(tempDir.name, "result.imfit")
    callString = "imfit %s " % noisedModelFitsName
    callString += "-c %s " % inputImfitModel.modelFileName
    callString += "--psf %s " % psfErrFitsName
    callString += "--ftol=1e-7 --gain=1 "
    callString += "--readnoise %1.2f " % noiseStd
    callString += "--save-params %s " % outImfitName
    callString += "--max-threads 4 "
    callString += " --nm "
    callString += ">/dev/null"
    print("Decomposing %s model" % noisedModelFitsName)
    subprocess.call(callString, shell=True)
    outImfitModel = ImfitModel(outImfitName)
    return outImfitModel


# parsing command line arguments
parser = argparse.ArgumentParser(description="Estimate decomposition errors.")
parser.add_argument("--threads", default=4, type=int,
                    help="Number of nodes.")
parser.add_argument("--niter", default=16, type=int,
                    help="Number of iterations.")
parser.add_argument("--noise", default=5, type=float,
                    help="Gaussian noise level (1 sigma).")
parser.add_argument("--psf", type=str, help="File with psf stats.")
parser.add_argument("--model", type=str, help="Imfit model to use.")
parser.add_argument("--out", type=str, default="results.dat",
                    help="Output file with results.")
args = parser.parse_args()


if not path.exists("workDir"):
    mkdir("workDir")


inputImfitModel = ImfitModel(args.model)
psf = PSF(args.psf)

with Pool(args.threads) as p:
    arguments = repeat((inputImfitModel, psf, args.noise),
                       args.niter)
    results = p.starmap(run_model_decomposition, arguments)

if path.exists(args.out):
    remove(args.out)
for res in results:
    res.model_to_text(args.out)
